// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");

// Setup server
const app = express();

// Enable CORS
app.use(cors());

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-krabbenborg.gfrcp6x.mongodb.net/b256_CourseAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Routes
app.use("/users", userRoutes)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// Server listening
app.listen(4000, () => console.log(`API is now onlline on port 4000`));
